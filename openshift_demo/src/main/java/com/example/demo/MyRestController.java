package com.example.demo;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping ("/api/demo")
public class MyRestController {
	
	@GetMapping
	public String getMessage() {
		return "Hello from REST API";
	}
}
